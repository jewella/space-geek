import test from 'ava';
import { handler as Skill } from '..';
import { GET_FACT_MESSAGE, HELP_MESSAGE, HELP_REPROMPT, STOP_MESSAGE, FACTS } from '../src/messages';
import Request from 'alexa-request';

test('Launch request should respond with a random fact', t => {
  const event = Request.launchRequest().build();

  return Skill(event).then(response => {
    let text = response.response.outputSpeech.text;
    let fact = text.substr(GET_FACT_MESSAGE.length + 1);
    t.not(FACTS.indexOf(fact), -1);
  });
});

test('Fact intent should respond with a random fact', t => {
  const event = Request.intent('fact').build();

  return Skill(event).then(response => {
    let text = response.response.outputSpeech.text;
    let fact = text.substr(GET_FACT_MESSAGE.length + 1);
    t.not(FACTS.indexOf(fact), -1);
  });
});

test('Help intent should respond back with the help message', t => {
  const event = Request.intent('AMAZON.HelpIntent').build();

  return Skill(event).then(response => {
    t.deepEqual(response, {
      version: '1.0',
      response: {
        shouldEndSession: false,
        outputSpeech: { type: 'PlainText', text: HELP_MESSAGE },
        reprompt: {
          outputSpeech: {
            type: 'PlainText', text: HELP_REPROMPT
          }
        }
      }
    });
  });
});

test('Cancel intent should respond back with the stop message', t => {
  const event = Request.intent('AMAZON.CancelIntent').build();

  return Skill(event).then(response => {
    t.deepEqual(response, {
      version: '1.0',
      response: {
        shouldEndSession: true,
        outputSpeech: { type: 'SSML', ssml: `<speak>${STOP_MESSAGE}</speak>` },
      }
    });
  });
});

  test('Stop intent should respond back with the stop message', t => {
  const event = Request.intent('AMAZON.StopIntent').build();

  return Skill(event).then(response => {
    t.deepEqual(response, {
      version: '1.0',
      response: {
        shouldEndSession: true,
        outputSpeech: { type: 'SSML', ssml: `<speak>${STOP_MESSAGE}</speak>` },
      }
    });
  });
});
