import { Skill, Launch, Intent } from 'alexa-annotations';
import { say, ask } from 'alexa-response';
import ssml from 'alexa-ssml-jsx';
import { GET_FACT_MESSAGE, FACTS, SKILL_NAME, HELP_MESSAGE, HELP_REPROMPT, STOP_MESSAGE } from './messages';

@Skill
export default class Spacegeek {

  @Launch
  launch() {
    return this.fact();
  }

  @Intent('fact')
  fact() {
    let fact = this.getRandomFact(FACTS);
    return say(`${GET_FACT_MESSAGE} ${fact}`).card({ title: SKILL_NAME, content: fact });
  }

  getRandomFact(facts) {
    return facts[Math.floor(Math.random()*facts.length)];
  }

  @Intent('AMAZON.HelpIntent')
  help() {
    return ask(HELP_MESSAGE).reprompt(HELP_REPROMPT);
  }

  @Intent('AMAZON.CancelIntent', 'AMAZON.StopIntent')
  stop() {
    return say(<speak>{STOP_MESSAGE}</speak>);
  }

}
